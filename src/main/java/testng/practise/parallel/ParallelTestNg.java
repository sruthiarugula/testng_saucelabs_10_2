package testng.practise.parallel;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

//import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;

public class ParallelTestNg {
	public WebDriver driver;

	public static final String USERNAME = "sruthiarugula";
	public static final String ACCESS_KEY = "d9e742cb-4b8c-4423-a4a3-350a9e42be6d";
	public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";

	@BeforeMethod
	public void beforeMethod() {
		DesiredCapabilities caps = DesiredCapabilities.chrome();
		caps.setCapability("platform", "Windows 10");
		caps.setCapability("version", "43.0");
		// caps.setCapability(capabilityName, value);
		// System.setProperty("webdriver.chrome.driver",
		// "C://driver//chromedriver.exe");
		try {
			driver = new RemoteWebDriver(new URL(URL), caps);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void openAmazonNavigationPage() {
		driver.get("https://www.amazon.com/");
		// driver.quit();
		System.out.println("hello from ama");

	}

	@Test
	public void openGoogleNavigationPage() {
		driver.get("https://www.google.com");
		System.out.println("hello from google");

	}

	@AfterMethod
	public void afterMethod() {
		// driver.close();
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);
		driver.quit();
		driver.close();
		// closeBrowser();
	}

}
